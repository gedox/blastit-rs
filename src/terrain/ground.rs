//
// ground.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//
use super::Block;
use crate::Coord;
use crate::TILE_SIZE;

use crate::random::Seed;

#[derive(Debug)]
pub struct Terrain {
    seed: Seed,
}
impl Terrain {
    pub fn new() -> Self {
        Self { seed: Seed::new() }
    }

    pub fn from_seed(seed: u64) -> Self {
        Self {
            seed: Seed::from_seed(seed),
        }
    }

    pub fn get_block_at(&self, x: Coord, y: Coord) -> Block {
        let xx = x.div_euclid(TILE_SIZE);
        let yy = y.div_euclid(TILE_SIZE);
        if xx & yy & 1 == 1 {
            Block::Wall
        } else if xx.abs() + yy.abs() > 5 {
            match self.seed.gen_random(xx, yy) % 7 {
                7 => Block::Wall,
                x if x < 3 => Block::Breakable,
                _ => Block::Empty,
            }
        } else {
            Block::Empty
        }
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
