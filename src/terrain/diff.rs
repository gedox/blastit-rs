//
// diff.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//
use super::Block;
use crate::Coord;
use crate::TILE_SIZE;
use fnv::FnvHashMap as Map;

#[derive(Debug)]
pub struct Terrain {
    map: Map<(i64, i64), Block>,
}

impl Terrain {
    pub fn new() -> Self {
        Self {
            map: Map::default(),
        }
    }

    pub fn set_block_at(&mut self, x: Coord, y: Coord, block: Block) {
        let xx = x.div_euclid(TILE_SIZE);
        let yy = y.div_euclid(TILE_SIZE);
        self.map.insert((xx, yy), block);
    }
    pub fn get_block_at(&self, x: Coord, y: Coord) -> Option<Block> {
        let xx = x.div_euclid(TILE_SIZE);
        let yy = y.div_euclid(TILE_SIZE);
        self.map.get(&(xx, yy)).cloned()
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
