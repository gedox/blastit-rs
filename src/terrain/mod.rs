//
// mod.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//

mod diff;
mod ground;

use crate::component::entity::Location;
use crate::Coord;
use crate::TILE_SIZE;

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Block {
    Empty,
    Wall,
    Breakable,
}

#[derive(Debug)]
pub struct Terrain {
    ground: ground::Terrain,
    diff: diff::Terrain,
}

impl Terrain {
    pub fn new() -> Self {
        Self {
            ground: ground::Terrain::new(),
            diff: diff::Terrain::new(),
        }
    }

    pub fn set_block_at(&mut self, x: Coord, y: Coord, block: Block) {
        self.diff.set_block_at(x, y, block);
    }
    pub fn get_block_at(&self, x: Coord, y: Coord) -> Block {
        self.diff
            .get_block_at(x, y)
            .unwrap_or_else(|| self.ground.get_block_at(x, y))
    }

    pub fn test_collision(&self, x: Coord, y: Coord) -> bool {
        let res = match self.get_block_at(x, y) {
            Block::Empty => false,
            _ => true,
        };
        res
    }

    pub fn test_collision_loc(&self, location: Location) -> bool {
        self.test_collision(location.x, location.y)
    }

    pub fn four_collision(&self, x: Coord, y: Coord) -> bool {
        self.test_collision(x, y)
            || self.test_collision(x + TILE_SIZE - Coord::from(1), y)
            || self.test_collision(x, y + TILE_SIZE - Coord::from(1))
            || self.test_collision(
                x + TILE_SIZE - Coord::from(1),
                y + TILE_SIZE - Coord::from(1),
            )
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
