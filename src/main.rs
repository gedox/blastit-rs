mod assets;
mod component;
mod controls;
mod coord;
mod gui;
mod random;
mod sounds;
mod system;
mod terrain;

use crate::component::control::Action;
use crate::gui::Event;
use crate::system::System;
use glutin_window::GlutinWindow;

type Coord = coord::Coord;
type Error = Box<dyn std::error::Error + 'static>;
type Controls = crate::controls::Controls<piston::keyboard::Key, Action>;

const TILE_SIZE: Coord = Coord::from(64);

fn main() -> Result<(), Error> {
    let mut world = hecs::World::new();
    let mut window = gui::GUIOpenGl::<GlutinWindow>::new(512, 512, "Hello world")?;
    let system = System::setup(&mut world)?;
    let music = sounds::CommandSink::default()?;
    while let Some(event) = window.next_event() {
        match event {
            Event::Mouse(button) => {
                system.run_mouse_systems(&mut world, button);
            }
            Event::Key(key) => {
                system.run_key_systems(&mut world, key);
            }
            Event::Update(update) => {
                system.run_update_systems(&mut world, update);
            }
            Event::Render(draw) => {
                let mut frame = window.arged(draw);
                system.run_draw_systems(&mut world, &mut frame);
                let index = frame.get_index();
                system.run_sound_systems(&mut world, &music, index);
            }
        }
    }
    Ok(())
}
