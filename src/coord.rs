//
// coord.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//

use std::cmp::Ordering;

const BITS: usize = 64;
type Inner = i64;

#[derive(PartialEq, Eq, Clone, Copy, Hash, Default)]
pub struct Coord(Inner);

struct DebugCoord(i64);

impl std::fmt::Debug for Coord {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        self.as_i64().fmt(f)
    }
}

impl Coord {
    pub fn abs(self) -> Self {
        Self(self.0.abs())
    }
    pub fn div_euclid(self, other: Self) -> i64 {
        self.0.div_euclid(other.0)
    }
    pub fn signum(self) -> i64 {
        self.0.signum()
    }
    pub fn as_f64(self) -> f64 {
        self.as_i64() as f64
    }
    fn as_i64(self) -> i64 {
        self.0 >> (std::mem::size_of::<Inner>() * 8 - BITS)
    }
    pub const fn from(t: i64) -> Self {
        Self((t as Inner) << (std::mem::size_of::<Inner>() * 8 - BITS))
    }
}

impl std::cmp::PartialOrd for Coord {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        (self.0.wrapping_sub(other.0)).partial_cmp(&0)
    }
}

impl std::cmp::Ord for Coord {
    fn cmp(&self, other: &Self) -> Ordering {
        (self.0.wrapping_sub(other.0)).cmp(&0)
    }
}
impl std::ops::Add for Coord {
    type Output = Self;
    fn add(self, other: Self) -> Self {
        Self(self.0.wrapping_add(other.0))
    }
}

impl std::ops::Sub for Coord {
    type Output = Self;
    fn sub(self, other: Self) -> Self {
        Self(self.0.wrapping_sub(other.0))
    }
}

impl std::ops::AddAssign for Coord {
    fn add_assign(&mut self, rhs: Self) {
        *self = *self + rhs;
    }
}

impl std::ops::SubAssign for Coord {
    fn sub_assign(&mut self, rhs: Self) {
        *self = *self - rhs;
    }
}

impl std::ops::Neg for Coord {
    type Output = Self;
    fn neg(self) -> Self {
        Self(-self.0)
    }
}

impl std::ops::Mul<i64> for Coord {
    type Output = Self;
    fn mul(self, rhs: i64) -> Self {
        Self(self.0 * rhs)
    }
}

impl std::ops::Div<i64> for Coord {
    type Output = Self;
    fn div(self, rhs: i64) -> Self {
        Self::from(self.as_i64().div_euclid(rhs))
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
