//
// assets.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//

pub static sprites: &[&[u8]] = &[
    include_bytes!("../assets/sprites/simple_sprite1.tga"),
    include_bytes!("../assets/sprites/simple_sprite2.tga"),
    include_bytes!("../assets/sprites/simple_sprite3.tga"),
    include_bytes!("../assets/sprites/simple_sprite4.tga"),
    include_bytes!("../assets/sprites/simple_sprite5.tga"),
];

pub static fonts: &[&[u8]] = &[include_bytes!("../assets/fonts/font.ttf")];

pub static sound_effects: &[&[u8]] = &[
    include_bytes!("../assets/sounds/explosion.wav"),
    include_bytes!("../assets/sounds/death.wav"),
];
pub static music: &[&[u8]] = &[
    include_bytes!("../assets/sounds/plains.ogg"),
    include_bytes!("../assets/sounds/frozen.ogg"),
    include_bytes!("../assets/sounds/town.ogg"),
    include_bytes!("../assets/sounds/lava.ogg"),
    include_bytes!("../assets/sounds/space.ogg"),
];

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
