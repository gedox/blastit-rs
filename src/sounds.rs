//
// sounds.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//

use crate::assets;
use crate::Error;
use rodio;
use rodio::Source;
use std::collections::BTreeMap as Map;
use std::io;
use std::sync::Arc;
use std::sync::mpsc::{channel, Sender, Receiver, TryRecvError};

pub type Tag= String;

const DIFF : f32 = 0.5/44100.;
pub struct Sound {
    data : Arc<[u8]>,
}

struct ClonableDecoder {
    data : Arc<[u8]>,
    decoder: rodio::source::SamplesConverter<rodio::decoder::Decoder<io::Cursor<Arc<[u8]>>>, f32>,
}

impl ClonableDecoder {
    fn new(data : &Arc<[u8]>) -> Result<Self, Error> {
        Ok(Self {
            data : Arc::clone(&data),
            decoder: rodio::decoder::Decoder::new(io::Cursor::new(Arc::clone(data)))?.convert_samples(),
        })
    }
}

impl Iterator for ClonableDecoder {
    type Item=f32;
    fn next(&mut self) -> Option<f32> {
        self.decoder.next()
    }
}

impl Clone for ClonableDecoder {
    fn clone(&self) -> Self {
        Self {
            data : Arc::clone(&self.data),
            decoder: rodio::decoder::Decoder::new(io::Cursor::new(Arc::clone(&self.data))).unwrap().convert_samples()
        }
    }
}

struct PlayingSound {
    iter : Box<Iterator<Item=f32> + Send + Sync + 'static>,
}

impl Sound {
    pub fn from(data : &[u8]) -> Result<Self, rodio::decoder::DecoderError> {
        Ok(Self {
            data : Arc::from(Vec::from(data)),
        })
    }

    pub fn get_playing_sound(&self) -> Result<PlayingSound, Error> {
        Ok(PlayingSound {
        iter: Box::new(ClonableDecoder::new(&self.data)?),
        })
    }
    pub fn get_playing_music(&self) -> Result<PlayingSound, Error> {
        Ok(PlayingSound {
        iter: Box::new(ClonableDecoder::new(&self.data)?.cycle()),
        })
    }
}

struct MusicParams {
    sound : PlayingSound,
    tag: Tag,
    volume : f32,
}

struct SoundParams {
    sound: PlayingSound,
}

impl SoundParams {
    fn new(sound : PlayingSound) -> Self {
        Self {
            sound,
        }
    }
}

enum Command {
    PlaySound(Tag),
    PlayMusic(Option<Tag>),
}

pub struct MusicSource {
    sounds : Map<Tag, Sound>,
    sounds_playing: Vec<SoundParams>,
    music_playing: Option<MusicParams>,
    fading : Vec<MusicParams>,
    receiver: Receiver<Command>,
}

impl Iterator for MusicSource {
    type Item=f32;
    fn next(&mut self) -> Option<f32> {
        match self.receiver.try_recv() {
            Ok(Command::PlaySound(tag)) => {
                self.sounds_playing.push(SoundParams::new(
                        self.sounds[&tag].get_playing_sound().unwrap()
                        ));
            }
            Ok(Command::PlayMusic(tag)) => {
                if self.music_playing.as_ref().map(|t| &t.tag) != tag.as_ref() {
                    if let Some(playing) = self.music_playing.take() {
                        self.fading.push(playing);
                    }

                    if let Some((n, params)) = self.fading.iter().enumerate()
                        .find(|x| Some(&x.1.tag) == tag.as_ref()) {
                        self.music_playing = Some(self.fading.swap_remove(n));
                    } else {
                        self.music_playing = tag.map(|tag| MusicParams {
                            sound : self.sounds[&tag].get_playing_music().unwrap(),
                            tag,
                            volume: 0.0,
                        })
                    }
                }
            }
            Err(TryRecvError::Disconnected) => {
                return None;
            }
            Err(TryRecvError::Empty) => (),
        }
        let mut out = 0.0;
        let sounds = &self.sounds;
        let mut remove_vec = Vec::new();
        for (n, params) in self.sounds_playing.iter_mut().enumerate() {
            if let Some(data) = params.sound.iter.next() {
                out += data;
            } else {
                remove_vec.push(n);
            }
        }
        for i in remove_vec.into_iter().rev() {
            self.sounds_playing.swap_remove(i);
        }
        self.fading.retain(|params| params.volume > 0.0);
        for (n, params) in self.fading.iter_mut().enumerate() {
            if let Some(data) = params.sound.iter.next() {
                out += data * params.volume;
            }
            if n == 0 {
                params.volume -= DIFF;
                if params.volume < 0.0 {
                    params.volume = 0.0;
                }
            }
        }
        for params in &mut self.music_playing {
            if let Some(data) = params.sound.iter.next() {
                out += data * params.volume;
            }
            params.volume += DIFF;
            if params.volume > 1.0 {
                params.volume = 1.0;
            }
        }
        Some(out)
    }
}

impl rodio::source::Source for MusicSource {
    
    fn current_frame_len(&self) -> Option<usize> {
        None
    }
    fn channels(&self) -> u16 {
        2
    }
    fn sample_rate(&self) -> u32 {
        44100
    }
    fn total_duration(&self) -> Option<std::time::Duration> {
        None
    }
}

pub struct CommandSink {
    sender : Sender<Command>,
}



impl MusicSource {
    pub fn new() -> Result<(Self, CommandSink), Error> {
        let (sender, receiver) = channel();
        Ok((Self {
            sounds: Map::new(),
            sounds_playing: Vec::new(),
            fading : Vec::new(),
            music_playing: None,
            receiver,
        }, CommandSink {
            sender
        }))
    }
    pub fn add_sound(&mut self, tag: impl Into<Tag>, sound: Sound) {
        self.sounds.insert(tag.into(), sound);
    }
    pub fn consume(self) -> Result<(), Error>{
        let device = rodio::default_output_device().ok_or("No device found")?;
        rodio::play_raw(&device, self);
        Ok(())
    }
}

impl CommandSink {
    pub fn default() -> Result<Self, Error> {
        let (mut source, sink) = MusicSource::new()?;
        source.add_sound("explosion", Sound::from(assets::sound_effects[0])?);
        source.add_sound("playerdeath", Sound::from(assets::sound_effects[1])?);
        use std::cell::Cell;
        source.add_sound("0", Sound::from(assets::music[0])?);
        source.add_sound("1", Sound::from(assets::music[1])?);
        source.add_sound("2", Sound::from(assets::music[2])?);
        source.add_sound("3", Sound::from(assets::music[3])?);
        source.add_sound("4", Sound::from(assets::music[4])?);
        source.consume()?;
        Ok(sink)
    }
    pub fn play_sound(&self, tag: impl Into<Tag>) {
        self.sender.send(Command::PlaySound(tag.into()));
    }
    pub fn play_music(&self, tag: impl Into<Tag>) {
        self.sender.send(Command::PlayMusic(Some(tag.into())));
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
