//
// draw.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//

use crate::component::*;
use crate::gui::GUI;
use crate::terrain::{Block, Terrain};
use crate::TILE_SIZE;
use fnv::FnvHashMap as Map;
use hecs::World;

pub fn set_camera<G: GUI>(world: &mut World, frame: &mut G) {
    for (_id, (location, _camera)) in world.query::<(&Location, &Camera)>().iter() {
        frame.set_camera(location);
    }
}
pub fn draw_entities<G: GUI>(world: &mut World, frame: &mut G) {
    for (_id, (location, draw, animated)) in world
        .query::<(&Location, &Draw, Option<&Animated>)>()
        .without::<Explosion>()
        .iter()
    {
        frame.draw_entity(&crate::gui::DrawParams {
            location,
            draw,
            animated,
        });
    }
}

pub fn draw_explosions<G: GUI>(world: &mut World, frame: &mut G) {
    let mut map = Map::default();
    for (_id, (location, animated)) in world
        .query::<(&Location, Option<&Animated>)>()
        .with::<Explosion>()
        .iter()
    {
        map.insert(*location, animated.cloned());
    }
    for (location, animated) in &map {
        let x = map.contains_key(&location.extend(&Direction::Left, TILE_SIZE))
            || map.contains_key(&location.extend(&Direction::Right, TILE_SIZE));
        let y = map.contains_key(&location.extend(&Direction::Up, TILE_SIZE))
            || map.contains_key(&location.extend(&Direction::Down, TILE_SIZE));
        let texture = if x && !y {
            7
        } else if y && !x {
            8
        } else {
            9
        };

        frame.draw_entity(&crate::gui::DrawParams {
            location,
            draw: &Draw { texture },
            animated: animated.as_ref(),
        });
    }
}
pub fn draw_terrain<G: GUI>(world: &mut World, frame: &mut G) {
    for (_id, terrain) in world.query::<&Terrain>().iter() {
        let camera_location = frame.get_camera();
        let camera_location = camera_location.snap(TILE_SIZE);
        for i in -33..33 {
            for j in -20..20 {
                let location = Location {
                    x: camera_location.x + TILE_SIZE * i,
                    y: camera_location.y + TILE_SIZE * j,
                };
                let draw_element = match terrain.get_block_at(location.x, location.y) {
                    Block::Empty => 5,
                    Block::Breakable => 2,
                    Block::Wall => 1,
                };
                frame.draw_entity(&crate::gui::DrawParams {
                    location: &location,
                    draw: &crate::component::draw::Draw {
                        texture: 5,
                    },
                    animated: None,
                });
                frame.draw_entity(&crate::gui::DrawParams {
                    location: &location,
                    draw: &crate::component::draw::Draw {
                        texture: draw_element,
                    },
                    animated: None,
                });
            }
        }
    }
}

pub fn draw_debug_info<G: GUI>(world: &mut World, frame: &mut G) {
    for (_id, counter) in world.query::<&Counter>().iter() {
        for (_id, (_camera, location)) in world.query::<(&Camera, &Location)>().iter() {
            frame.draw_text(&format!("{:#?}, hero at: {:#?}", counter, location));
        }
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
