//
// sounds.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//

use crate::component::*;
use crate::sounds::CommandSink;
use hecs::World;

pub fn play_sounds(world: &mut World, bag: &CommandSink) {
    let mut trash = Vec::new();
    for (id, sound) in world.query::<&Sound>().iter() {
        bag.play_sound(sound.0);
        trash.push(id);
    }
    for sound in trash {
        world.despawn(sound);
    }
}
pub fn play_music(world: &mut World, bag: &CommandSink, index: usize) {
    let tag = match index {
        0 => "0",
        1 => "1",
        2 => "2",
        3 => "3",
        4 => "4",
        _ => unimplemented!(),
    };
    bag.play_music(tag);
}

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
