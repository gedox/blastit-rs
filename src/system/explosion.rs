//
// explosive.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//

use crate::component::*;
use crate::terrain::Terrain;
use crate::TILE_SIZE;
use hecs::World;

const TICKS_NEXT: usize = 1;

pub fn bombing(world: &mut World) {
    let mut bombs = Vec::new();
    for (_id, (bomber, location, mut liquid)) in world
        .query::<(&mut Bomber, &Location, Option<&mut Liquid>)>()
        .iter()
    {
        if let Some(liquid) = liquid.as_mut() {
            if liquid.next_dropping == 0 {
                liquid.next_dropping = liquid.reset;
                bomber.dropping = true;
            }
            liquid.next_dropping -= 1;
        }
        if bomber.dropping {
            bombs.push(location.snap(TILE_SIZE));
        }
        bomber.dropping = false;
    }
    for (_id, terrain) in world.query::<&Terrain>().iter() {
        bombs.retain(|&location| !terrain.test_collision_loc(location));
    }
    if bombs.len() > 0 {
        for (_id, (location, _solid)) in world.query::<(&Location, &Solid)>().iter() {
            bombs.retain(|bloc| location != bloc);
        }
    }
    
    for location in bombs {
        world.spawn((
            Solid,
            Fragile,
            Draw { texture: 3 },
            OnDeathExplode {
                distance: 3,
                strength: 1,
                speed: 1,
            },
            Dying {
                ticks_remaining: 180,
            },
            OnDeathSound("explosion"),
            location,
        ));
    }
}

pub fn expansion(world: &mut World) {
    let mut new_explosions = Vec::new();
    let dead_explosions = Vec::new();
    let mut new_terrain: isize = 0;
    {
        let mut lock = world.query::<&mut Terrain>();
        let mut terrain = lock.iter().next();
        for (_id, (explosion, location, dying)) in world
            .query::<(&mut Explosion, &Location, Option<&Dying>)>()
            .iter()
        {
            if explosion.ticks_next > 0 && explosion.distance_remaining > 0 {
                explosion.ticks_next -= 1;
                if explosion.ticks_next == 0 {
                    let new_location = explosion.direction.extend(location, TILE_SIZE);
                    let mut distance_remaining = explosion.distance_remaining - 1;
                    if let Some(&mut (_id, ref mut terrain)) = terrain.as_mut() {
                        use crate::terrain::Block;
                        let _b = false;
                        match terrain.get_block_at(new_location.x, new_location.y) {
                            Block::Wall => {
                                continue;
                            }
                            Block::Breakable => {
                                terrain.set_block_at(new_location.x, new_location.y, Block::Empty);
                                distance_remaining = 0;
                                new_terrain += 1;
                            }
                            _ => (),
                        }
                    }
                    new_explosions.push((
                        (
                            new_location,
                            Explosion {
                                distance_remaining,
                                ticks_next: explosion.total_ticks_next,
                                ..explosion.clone()
                            },
                            Draw { texture: 7 },
                            Spiky,
                        ),
                        dying.cloned(),
                    ));
                }
            }
        }
    }

    let mut new_entities: isize = 0;
    for expl in new_explosions {
        let entity = world.spawn(expl.0);
        if let Some(dying) = expl.1 {
            world.insert_one(entity, dying);
        }
        new_entities += 1;
    }
    for dead in dead_explosions {
        world.despawn(dead);
        new_entities -= 1;
    }
    for (_id, counter) in world.query::<&mut Counter>().iter() {
        counter.entities = counter.entities.wrapping_add(new_entities as usize);
        counter.terrain = counter.terrain.wrapping_add(new_terrain as usize);
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
