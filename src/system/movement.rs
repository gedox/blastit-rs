//
// move.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//

use crate::component::control::Action;
use crate::component::control::ALL_DIRECTION_ACTIONS;
use crate::component::control::{Input, Moving};
use crate::component::debug::Automate;
use crate::component::draw::Animated;
use crate::component::entity::{Collides, Direction, Location, Solid};
use crate::component::explosion::Bomber;
use crate::terrain::Terrain;
use crate::Controls;
use crate::TILE_SIZE;
use hecs::World;

pub fn control(world: &mut World) {
    if let Some((_, keybag)) = world.query::<&mut Controls>().iter().next() {
        for (_id, (_input, moving)) in world.query::<(&Input, &mut Moving)>().iter() {
            moving.direction = match keybag.first_pressed(ALL_DIRECTION_ACTIONS).cloned() {
                Some(Action::Direction(x)) => Some(x),
                None => None,
                _ => unreachable!(),
            }
        }
        for (_id, (_input, bomber)) in world.query::<(&Input, &mut Bomber)>().iter() {
            bomber.dropping = keybag.is_just_pressed(&Action::Drop);
        }
        keybag.begin_tick();
    }
    for (_id, (_automate, moving)) in world.query::<(&Automate, &mut Moving)>().iter() {
        moving.direction = Some(Direction::Up);
    }
}

pub fn movement(world: &mut World) {
    for (_id, (moving, location, collides)) in world
        .query::<(&Moving, &mut Location, Option<&Collides>)>()
        .iter()
    {
        if let Some(direction) = moving.direction {
            let new_pos = location.extend(&direction, moving.speed);
            // projection is the square we're moving towards
            let projection_a = location
                .extend(&direction, TILE_SIZE)
                .extend(&direction.turn(&Direction::Right), TILE_SIZE / 2)
                .snap(TILE_SIZE);
            let projection_b = location
                .extend(&direction, TILE_SIZE)
                .extend(&direction.turn(&Direction::Left), TILE_SIZE / 2)
                .snap(TILE_SIZE);
            let projection_c = location.snap(TILE_SIZE).extend(&direction, TILE_SIZE);
            if collides.is_some() {
                let mut test_a = false;
                let mut test_b = false;
                if new_pos.collide(&projection_a, TILE_SIZE)
                    && !location.collide(&projection_a, TILE_SIZE)
                {
                    test_a = true;
                }
                if new_pos.collide(&projection_b, TILE_SIZE)
                    && !location.collide(&projection_b, TILE_SIZE)
                {
                    test_b = true;
                }
                let mut res_a = false;
                let mut res_b = false;
                let mut res_c = false;
                if test_a || test_b {
                    if let Some((_id, terrain)) = world.query::<&Terrain>().iter().next() {
                        res_a |= test_a && terrain.test_collision_loc(*&projection_a);
                        res_b |= test_b && terrain.test_collision_loc(*&projection_b);
                        res_c |= terrain.test_collision_loc(*&projection_c);
                    }
                    for (_id, (_solid, location)) in world.query::<(&Solid, &Location)>().iter() {
                        res_a |= test_a && location.collide(&projection_a, TILE_SIZE);
                        res_b |= test_b && location.collide(&projection_b, TILE_SIZE);
                        res_c |= location.collide(&projection_c, TILE_SIZE);
                    }
                }
                if res_a || res_b {
                    let new_location = location.limit(&location.snap(TILE_SIZE), moving.speed);
                    if !res_c || new_location == location.snap(TILE_SIZE) {
                        *location = new_location;
                    }
                    continue;
                }
            }
            *location = new_pos;
        }
    }
}

pub fn animate(world: &mut World) {
    for (_id, (moving, animated)) in world.query::<(&Moving, &mut Animated)>().iter() {
        if let Some(direction) = moving.direction {
            animated.direction = direction;
            animated.animation_step += 1;
        } else {
            animated.animation_step = 0;
        }
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
