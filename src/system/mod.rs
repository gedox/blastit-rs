//
// mod.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//

mod death;
mod draw;
mod explosion;
mod movement;
mod sounds;

use crate::component::*;
use crate::gui::KeyArgs;
use crate::gui::MouseArgs;
use crate::gui::GUI;
use crate::sounds::CommandSink;
use crate::terrain::Terrain;
use crate::Controls;
use crate::Coord;
use crate::Error;
use piston::keyboard::Key;
use piston::ButtonState;
use piston::UpdateArgs;

pub struct System;

impl System {
    pub fn setup(world: &mut hecs::World) -> Result<Self, Error> {
        let mut keybag = Controls::new();
        keybag.register(Key::W, Action::Direction(Direction::Up));
        keybag.register(Key::Z, Action::Direction(Direction::Up));
        keybag.register(Key::S, Action::Direction(Direction::Down));
        keybag.register(Key::A, Action::Direction(Direction::Left));
        keybag.register(Key::Q, Action::Direction(Direction::Left));
        keybag.register(Key::D, Action::Direction(Direction::Right));
        keybag.register(Key::Space, Action::Drop);
        world.spawn((keybag,));
        world.spawn((Terrain::new(),));
        world.spawn((Counter::default(),));

        world.spawn((
            Fragile,
            Hero,
            Camera,
            Bomber { dropping: true },
            Location::default(),
            Moving {
                speed: Coord::from(3),
                direction: None,
            },
            Input,
            //            Liquid {
            //                next_dropping: 360,
            //                reset : 180,
            //            },
            Reset {
                location: Location::default(),
            },
            Draw { texture: 0 },
            Animated {
                animation_step: 0,
                direction: Direction::Up,
            },
            OnDeathSound ("playerdeath"),
            Collides,
        ));
        Ok(Self)
    }

    pub fn run_key_systems(&self, world: &mut hecs::World, key: KeyArgs) {
        if let Some((_, keybag)) = world.query::<&mut Controls>().iter().next() {
            match key.state {
                ButtonState::Press => keybag.press(&key.button),
                ButtonState::Release => keybag.release(&key.button),
            }
        }
    }

    pub fn run_mouse_systems(&self, _world: &mut hecs::World, _mouse: MouseArgs) {}

    pub fn run_update_systems(&self, world: &mut hecs::World, _args: UpdateArgs) {
        movement::control(world);
        movement::movement(world);
        movement::animate(world);
        explosion::bombing(world);
        death::death_hero(world);
        death::death_fragile(world);
        death::death_dying(world);
        death::handle_death_and_destruction(world);
        explosion::expansion(world);
    }

    pub fn run_draw_systems<G: GUI>(
        &self,
        world: &mut hecs::World,
        frame: &mut G,
    ) {
        draw::set_camera(world, frame);
        draw::draw_terrain(world, frame);
        draw::draw_entities(world, frame);
        draw::draw_explosions(world, frame);
        draw::draw_debug_info(world, frame);
    }

    pub fn run_sound_systems(
        &self,
        world: &mut hecs::World,
        music: &CommandSink,
        index: usize,
    ) {
        sounds::play_sounds(world, music);
        sounds::play_music(world, music, index);
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
