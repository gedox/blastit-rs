//
// death.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//

use crate::component::*;
use crate::Coord;
use crate::TILE_SIZE;
use hecs::World;

const TOLERANCE: Coord = Coord::from(10);

pub fn death_hero(world: &mut World) {
    let mut dead_list = Vec::new();
    for (id, (location, _hero)) in world.query::<(&mut Location, &Hero)>().iter() {
        for (_id, (enemy_location, _villain)) in world.query::<(&Location, &Villain)>().iter() {
            if location.collide(enemy_location, TILE_SIZE - TOLERANCE) {
                dead_list.push(id);
            }
        }
    }

    for dead in dead_list {
        world.insert_one(dead, Dead);
    }
}

pub fn death_dying(world: &mut World) {
    let mut dead_list = Vec::new();
    for (id, dying) in world.query::<&mut Dying>().iter() {
        dying.ticks_remaining = dying.ticks_remaining.saturating_sub(1);
        if dying.ticks_remaining == 0 {
            dead_list.push(id);
        }
    }

    for dead in dead_list {
        world.insert_one(dead, Dead);
    }
}

pub fn death_fragile(world: &mut World) {
    let mut dead_list = Vec::new();
    for (id, (location, _fragile)) in world.query::<(&mut Location, &Fragile)>().iter() {
        for (_id, (enemy_location, _spiky)) in world.query::<(&Location, &Spiky)>().iter() {
            if location.collide(enemy_location, TILE_SIZE - TOLERANCE) {
                dead_list.push(id);
            }
        }
    }

    for dead in dead_list {
        world.insert_one(dead, Dead);
    }
}

pub fn handle_death_and_destruction(world: &mut World) {
    let mut sound_list = Vec::new();
    let mut explode_list = Vec::new();
    let mut remove_list = Vec::new();
    let mut resurrect_list = Vec::new();
    for (_id, (location, explode)) in world
        .query::<(&Location, &OnDeathExplode)>()
        .with::<Dead>()
        .iter()
    {
        explode_list.push((explode.clone(), location.clone()));
    }
    for (_id, sound) in world.query::<&OnDeathSound>().with::<Dead>().iter() {
        sound_list.push(sound.clone());
    }
    for (id, val) in world
        .query::<Option<(&mut Location, &Reset)>>()
        .with::<Dead>()
        .iter()
    {
        if let Some((location, reset)) = val {
            *location = reset.location;
            resurrect_list.push(id);
        } else {
            remove_list.push(id);
        }
    }

    for sound in sound_list {
        world.spawn((Sound(sound.0),));
    }
    for (explode, location) in explode_list {
        use crate::component::entity::ALL_DIRECTIONS;
        for direction in ALL_DIRECTIONS {
            world.spawn((
                location.clone(),
                Draw { texture: 4 },
                Spiky,
                Explosion {
                    direction: **direction,
                    total_ticks_next: explode.speed,
                    distance_remaining: explode.distance,
                    ticks_next: explode.speed,
                },
                Dying {
                    ticks_remaining: 90,
                },
            ));
        }
    }
    for remove in remove_list {
        world.despawn(remove);
    }
    for resurrect in resurrect_list {
        world.remove::<(Dead,)>(resurrect);
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
