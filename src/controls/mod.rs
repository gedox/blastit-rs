//
// mod.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//

use std::cmp::PartialEq;
use std::collections::BTreeMap as Map;

pub struct Controls<X, T> {
    keymap: Map<X, T>,
    pressed: Vec<T>,
    pressed_tick: Vec<T>,
}

impl<X: Eq + Ord, T: PartialEq + Clone> Controls<X, T> {
    pub fn new() -> Self {
        Self {
            keymap: Map::new(),
            pressed: Vec::new(),
            pressed_tick: Vec::new(),
        }
    }

    pub fn register(&mut self, key: X, t: T) {
        self.keymap.insert(key, t);
    }

    pub fn deregister(&mut self, key: &X) {
        self.keymap.remove(key);
    }

    pub fn press(&mut self, key: &X) {
        if let Some(t_ref) = self.keymap.get(key) {
            if self.pressed.iter().all(|x| x != t_ref) {
                self.pressed.push(t_ref.clone());
            }
            if self.pressed_tick.iter().all(|x| x != t_ref) {
                self.pressed_tick.push(t_ref.clone());
            }
        }
    }

    pub fn release(&mut self, key: &X) {
        if let Some(t_ref) = self.keymap.get(key) {
            self.pressed.retain(|x| x != t_ref);
        }
    }

    pub fn is_pressed(&self, t: &T) -> bool {
        self.pressed.iter().any(|x| x == t)
    }

    pub fn first_pressed(&self, t: &[&T]) -> Option<&T> {
        for x in self.pressed.iter().rev() {
            for &y in t {
                if x == y {
                    return Some(x);
                }
            }
        }
        None
    }
    pub fn is_just_pressed(&self, t: &T) -> bool {
        self.pressed_tick.iter().any(|x| x == t)
    }
    pub fn begin_tick(&mut self) {
        self.pressed_tick.clear();
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
