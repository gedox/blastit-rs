//
// textures.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//

use crate::component::entity::Direction;
use crate::random::Seed;
use crate::Coord;
use std::collections::BTreeMap as Map;

pub type TextureBag = TextureBagImpl<[f64; 4]>;

const ANIMATION_STEPS: usize = 20;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
struct TextureDef {
    id: u64,
    direction: Option<Direction>,
}

struct TextureImpl<X> {
    color: X,
}
pub struct TextureBagImpl<X> {
    map: Map<TextureDef, Vec<TextureImpl<X>>>,
}

impl<X> TextureBagImpl<X> {
    pub fn new() -> Self {
        Self { map: Map::new() }
    }
    pub fn insert_token(&mut self, id: u64, direction: Option<Direction>, color: X) {
        self.map
            .entry(TextureDef { id, direction })
            .or_insert_with(Vec::new)
            .push(TextureImpl { color });
    }

    pub fn entity_token(&self, id: u64, direction: Direction, step: u64) -> &X {
        let array = self
            .map
            .get(&TextureDef {
                id,
                direction: Some(direction),
            })
            .unwrap();
        if step == 0 || array.len() == 1 {
            &array[0].color
        } else {
            &array[(((step as usize) / ANIMATION_STEPS) % (array.len() - 1)) + 1].color
        }
    }
    pub fn wall_token(&self, id: u64) -> &X {
        let array = self
            .map
            .get(&TextureDef {
                id,
                direction: None,
            })
            .unwrap();
        &array[0].color
    }
}

const FIELD_SIZE: Coord = Coord::from(0x400);

pub struct TextureField {
    seed: Seed,
    options: u64,
    field_size: Coord,
}

impl TextureField {
    pub fn new(options: usize) -> Self {
        Self {
            seed: Seed::new(),
            options: options as u64,
            field_size: FIELD_SIZE,
        }
    }

    pub fn get_option(&self, x: Coord, y: Coord) -> usize {
        let xx = x.div_euclid(self.field_size);
        let yy = y.div_euclid(self.field_size);
        let mut max_strength = 0.0;
        let mut chosen_pillar = 0;
        for &(px, py) in &[(xx, yy), (xx, yy + 1), (xx + 1, yy), (xx + 1, yy + 1)] {
            let dx = (x - self.field_size * px).as_f64();
            let dy = (y - self.field_size * py).as_f64();
            let pillar = self.seed.gen_random(px, py) % self.options;
            let height = (self.seed.gen_random(px, py) / self.options) as f64;
            let distance = dx * dx + dy * dy + 1.0;
            let strength = height / distance;
            if strength > max_strength {
                max_strength = strength;
                chosen_pillar = pillar;
            }
        }
        return chosen_pillar as usize;
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
