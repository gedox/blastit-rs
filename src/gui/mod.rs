//
// mod.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//
mod textures;

use crate::assets::fonts;
use crate::assets::sprites;
use crate::component;
use crate::component::entity::Direction;
use crate::Error;
use crate::TILE_SIZE;
use ::image::load;
use ::image::ImageFormat;
use graphics::image;
use graphics::rectangle;
use opengl_graphics::GlGraphics;
use opengl_graphics::OpenGL;
use opengl_graphics::Texture;
use piston::event_loop::{EventSettings, Events};
use piston::Window;
use piston::{RenderArgs, UpdateArgs};

const FONT_SIZE: usize = 14;
const FONT_SEP: usize = 2;

pub struct DrawParams<'a> {
    pub location: &'a component::entity::Location,
    pub draw: &'a component::draw::Draw,
    pub animated: Option<&'a component::draw::Animated>,
}

pub struct MouseArgs {
    pub state: piston::ButtonState,
    pub button: piston::mouse::MouseButton,
    pub scancode: Option<i32>,
}
pub struct KeyArgs {
    pub state: piston::ButtonState,
    pub button: piston::keyboard::Key,
    pub scancode: Option<i32>,
}
pub enum Event {
    Mouse(MouseArgs),
    Key(KeyArgs),
    Update(UpdateArgs),
    Render(RenderArgs),
}

pub trait GUI {
    fn clear(&mut self);
    fn draw_entity(&mut self, param: &DrawParams);
    fn set_camera(&mut self, location: &component::entity::Location);
    fn get_camera(&self) -> component::entity::Location;
    fn draw_text(&mut self, text: &str);
}

pub struct GUIOpenGl<W: Window> {
    window: W,
    events: Events,
    backend: GlGraphics,
    texturebag: textures::TextureBag,
    glyphs: opengl_graphics::GlyphCache<'static>,
    textures: Vec<opengl_graphics::Texture>,
    texturefield: textures::TextureField,
}

pub struct ArgedOpenGl<'a, W: Window> {
    gui: &'a mut GUIOpenGl<W>,
    render_args: RenderArgs,
    camera_location: component::entity::Location,
    camera_parameters: component::draw::Camera,
    context: graphics::Context,
}

impl<'a, W: Window> Drop for ArgedOpenGl<'a, W> {
    fn drop(&mut self) {
        self.gui.backend.draw_end();
    }
}

fn setup_texturebag(bag: &mut textures::TextureBag) {
    for (n, &dir) in [
        Direction::Up,
        Direction::Down,
        Direction::Right,
        Direction::Left,
    ]
    .iter()
    .enumerate()
    {
        let x = n as f64 * 30.0;
        let _yy = (n / 2) as f64 * 30.0 + 30.0;
        bag.insert_token(0, Some(dir), [x, 0.0, 30.0, 30.0]);
        for &y in &[0, 1, 0, 2] {
            let y = y as f64 * 30.0;
            bag.insert_token(0, Some(dir), [x, y, 30.0, 30.0]);
        }
    }
    bag.insert_token(1, None, [150.0, 0.0, 30.0, 30.0]);
    bag.insert_token(2, None, [150.0, 30.0, 30.0, 30.0]);
    bag.insert_token(3, None, [120.0, 0.0, 30.0, 30.0]);
    bag.insert_token(5, None, [150.0, 60.0, 30.0, 30.0]);
    bag.insert_token(7, None, [330.0, 60.0, 30.0, 30.0]);
    bag.insert_token(8, None, [330.0, 30.0, 30.0, 30.0]);
    bag.insert_token(9, None, [330.0, 0.0, 30.0, 30.0]);
}

impl<W: Window + piston::BuildFromWindowSettings> GUIOpenGl<W> {
    pub fn new(w: u32, h: u32, name: &str) -> Result<Self, Error> {
        let opengl = OpenGL::V2_1;

        //Create an Glutin window.
        let window = piston::WindowSettings::new(name, [w, h])
            .graphics_api(opengl)
            .exit_on_esc(true)
            .vsync(false)
            .build()?;
        let mut texturebag = textures::TextureBag::new();
        setup_texturebag(&mut texturebag);
        use std::io::Cursor;
        let textures = sprites
            .iter()
            .map(|&i| load(Cursor::new(i), ImageFormat::Tga))
            .map(|r| {
                r.map(|i| {
                    Texture::from_image(&i.to_rgba(), &opengl_graphics::TextureSettings::new())
                })
            })
            .collect::<Result<Vec<_>, _>>()?;

        Ok(Self {
            window,
            events: Events::new(EventSettings::new()),
            backend: GlGraphics::new(opengl),
            texturebag,
            glyphs: opengl_graphics::GlyphCache::from_bytes(
                fonts[0],
                (),
                opengl_graphics::TextureSettings::new(),
            )
            .map_err(|()| "Cannot_load font")?,
            texturefield: textures::TextureField::new(textures.len()),
            textures,
        })
    }

    pub fn arged(&mut self, render_args: RenderArgs) -> ArgedOpenGl<W> {
        let mut res = ArgedOpenGl {
            context: self.backend.draw_begin(render_args.viewport()),
            gui: self,
            render_args,
            camera_location: component::entity::Location::default(),
            camera_parameters: component::draw::Camera::default(),
        };
        res.clear();
        res
    }

    pub fn next_event(&mut self) -> Option<Event> {
        use piston::{ButtonEvent, RenderEvent, UpdateEvent};
        while let Some(e) = self.events.next(&mut self.window) {
            if let Some(args) = e.button_args() {
                match args.button {
                    piston::Button::Keyboard(key) => {
                        return Some(Event::Key(KeyArgs {
                            state: args.state,
                            button: key,
                            scancode: args.scancode,
                        }));
                    }
                    piston::Button::Mouse(mouse) => {
                        return Some(Event::Mouse(MouseArgs {
                            state: args.state,
                            button: mouse,
                            scancode: args.scancode,
                        }));
                    }
                    _ => (),
                }
            }
            if let Some(args) = e.render_args() {
                return Some(Event::Render(args));
            }
            if let Some(args) = e.update_args() {
                return Some(Event::Update(args));
            }
        }
        None
    }
}

impl<'a, W: Window> ArgedOpenGl<'a, W> {
    pub fn get_index(&self) -> usize {
        self .gui .texturefield
            .get_option(self.camera_location.x, self.camera_location.y)
    }
}

impl<'a, W: Window> GUI for ArgedOpenGl<'a, W> {
    fn clear(&mut self) {
        graphics::clear([0.5; 4], &mut self.gui.backend);
    }
    fn set_camera(&mut self, location: &component::entity::Location) {
        self.camera_location = location.clone();
    }
    fn get_camera(&self) -> component::entity::Location {
        self.camera_location.clone()
    }

    fn draw_entity(&mut self, param: &DrawParams) {
        let (halfw, halfh) = (
            self.render_args.window_size[0] / 2.0,
            self.render_args.window_size[1] / 2.0,
        );
        let relative_x = ((param.location.x - self.camera_location.x) / 2).as_f64() + halfw;
        let relative_y = ((param.location.y - self.camera_location.y) / 2).as_f64() + halfh;

        let src_rect = if let Some(animated) = param.animated {
            self.gui.texturebag.entity_token(
                param.draw.texture,
                animated.direction,
                animated.animation_step,
            )
        } else {
            self.gui.texturebag.wall_token(param.draw.texture)
        };
        let square = rectangle::centered_square(relative_x, relative_y, TILE_SIZE.as_f64() / 4.0);
        let image = image::Image::new().rect(square).src_rect(*src_rect);
        let index = self
            .gui
            .texturefield
            .get_option(param.location.x, param.location.y);
        image.draw(
            &self.gui.textures[index],
            &self.context.draw_state,
            self.context.transform,
            &mut self.gui.backend,
        );
    }
    fn draw_text(&mut self, text: &str) {
        let (_halfw, _halfh) = (
            self.render_args.window_size[0] / 2.0,
            self.render_args.window_size[1] / 2.0,
        );
        let textbox = graphics::Text::new_color([1.0, 0.0, 0.0, 1.0], FONT_SIZE as u32);
        use graphics::Transformed;
        for (n, text) in text.split("\n").enumerate() {
            textbox.draw(
                text,
                &mut self.gui.glyphs,
                &self.context.draw_state,
                self.context
                    .transform
                    .trans(0., ((FONT_SIZE + FONT_SEP) * (n + 1)) as f64),
                &mut self.gui.backend,
            );
        }
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
