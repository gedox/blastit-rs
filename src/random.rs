//
// random.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//

use std::collections::hash_map::DefaultHasher;
use std::hash::Hasher;
#[derive(Debug)]
pub struct Seed(u64);

impl Seed {
    pub fn new() -> Self {
        Self(std::time::Instant::now().elapsed().as_nanos() as _)
    }
    pub fn another(&self) -> Self {
        Self(self.0.wrapping_add(1))
    }
    pub fn from_seed(seed: u64) -> Self {
        Self(seed)
    }
    pub fn gen_random(&self, x: i64, y: i64) -> u64 {
        let mut hasher = DefaultHasher::default();
        hasher.write_u64(self.0);
        hasher.write_i64(x);
        hasher.write_i64(y);
        hasher.finish()
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
