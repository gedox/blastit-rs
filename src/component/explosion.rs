//
// explosion.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//
use crate::component::entity::Direction;

#[derive(Debug, Clone)]
pub struct Bomber {
    pub dropping: bool,
}

#[derive(Debug, Clone)]
pub struct Liquid {
    pub next_dropping: usize,
    pub reset: usize,
}

#[derive(Debug, Clone)]
pub struct OnDeathExplode {
    pub strength: usize,
    pub distance: usize,
    pub speed: usize,
}

#[derive(Debug, Clone)]
pub struct Explosion {
    pub direction: Direction,
    pub total_ticks_next: usize,
    pub ticks_next: usize,
    pub distance_remaining: usize,
}

#[derive(Debug, Clone)]
pub struct Breakable {
    pub strength: usize,
}

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
