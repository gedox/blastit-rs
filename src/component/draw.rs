//
// draw.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//

use crate::component::entity::Direction;

pub struct Draw {
    pub texture: u64,
}

#[derive(Clone)]
pub struct Animated {
    pub animation_step: u64,
    pub direction: Direction,
}

#[derive(Default)]
pub struct Camera;

#[derive(Clone, Copy)]
pub struct Sound(pub &'static str);

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
