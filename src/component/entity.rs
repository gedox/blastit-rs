//
// entity.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//

use crate::Coord;

pub static ALL_DIRECTIONS: &[&Direction] = &[
    &Direction::Up,
    &Direction::Right,
    &Direction::Down,
    &Direction::Left,
];

#[derive(Clone, Copy, PartialEq, PartialOrd, Eq, Ord, Debug)]
pub enum Direction {
    Up,
    Right,
    Down,
    Left,
}

impl Direction {
    pub fn extend(&self, location: &Location, distance: Coord) -> Location {
        let mut dx = Coord::from(0);
        let mut dy = Coord::from(0);
        match self {
            Self::Up => {
                dy -= distance;
            }
            Self::Down => {
                dy += distance;
            }
            Self::Left => {
                dx -= distance;
            }
            Self::Right => {
                dx += distance;
            }
        }
        Location {
            x: location.x + dx,
            y: location.y + dy,
        }
    }
    pub fn turn(&self, direction: &Direction) -> Direction {
        let a = *self as usize;
        let b = *direction as usize;
        *ALL_DIRECTIONS[(a + b) % 4]
    }
}

#[derive(Default, Clone, Copy, PartialEq, Eq, Debug, Hash)]
pub struct Location {
    pub x: Coord,
    pub y: Coord,
}

impl Location {
    pub fn snap(&self, tile_size: Coord) -> Self {
        Self {
            x: tile_size * (self.x + tile_size / 2).div_euclid(tile_size),
            y: tile_size * (self.y + tile_size / 2).div_euclid(tile_size),
        }
    }
    pub fn collide(&self, other: &Self, tile_size: Coord) -> bool {
        (self.x - other.x) > -tile_size
            && (other.x - self.x) > -tile_size
            && (self.y - other.y) > -tile_size
            && (other.y - self.y) > -tile_size
    }
    pub fn extend(&self, direction: &Direction, distance: Coord) -> Location {
        direction.extend(self, distance)
    }
    pub fn limit(&self, destination: &Location, distance: Coord) -> Location {
        let mut diff_x = destination.x - self.x;
        let mut diff_y = destination.y - self.y;
        if -diff_x.abs() < -distance {
            diff_x = distance * diff_x.signum();
        }
        if -diff_y.abs() < -distance {
            diff_y = distance * diff_y.signum();
        }
        Location {
            x: self.x + diff_x,
            y: self.y + diff_y,
        }
    }
}

pub struct Solid;

pub struct Collides;
pub struct Reset {
    pub location: Location,
}

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
