//
// control.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//

use crate::component::entity::Direction;
use crate::Coord;

pub static ALL_DIRECTION_ACTIONS: &[&Action] = &[
    &Action::Direction(Direction::Up),
    &Action::Direction(Direction::Down),
    &Action::Direction(Direction::Left),
    &Action::Direction(Direction::Right),
];

#[derive(Clone, PartialEq)]
pub enum Action {
    Direction(crate::component::entity::Direction),
    Drop,
}

pub struct Moving {
    pub speed: Coord,
    pub direction: Option<Direction>,
}

pub struct Input;

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
