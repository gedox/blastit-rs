//
// effect.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//

pub enum Effect {
    
}

pub struct Effected {
    effects : Vec<Effect>
}

pub struct Effecting {
    
}

pub struct Token;

pub struct Pickup;


#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn it_works() {
	}
}
