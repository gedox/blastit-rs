//
// component.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//

pub mod control;
pub mod death;
pub mod debug;
pub mod draw;
pub mod entity;
pub mod explosion;

pub use control::*;
pub use death::*;
pub use debug::*;
pub use draw::*;
pub use entity::*;
pub use explosion::*;

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
