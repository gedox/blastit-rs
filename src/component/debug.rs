//
// debug.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//

#[derive(Debug, Default)]
pub struct Counter {
    pub entities: usize,
    pub terrain: usize,
}

pub struct Automate;

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
