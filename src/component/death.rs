//
// death.rs
// Copyright (C) 2020 gerwin <gerwin@gerwin-pc>
// Distributed under terms of the MIT license.
//

pub struct Spiky;

pub struct Villain;

pub struct Hero;

pub struct Fragile;

pub struct Dead;

#[derive(Debug, Clone)]
pub struct Dying {
    pub ticks_remaining: usize,
}

#[derive(Clone, Copy)]
pub struct OnDeathSound(pub &'static str);

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {}
}
